# Estimation of RF to DC Converter Power Conversion Efficiency Sensitivity to Component Parameters


## Summary

The present example demonstrates the use of high-performance computing (HPC) estimate the sensitivity of RF-DC converter power conversion efficiency (PCE) to small variations in the converter circuit parameter values. The RF-DC converter under study can be employed in wireless power transfer (WPT) systems where it converts the energy of an incoming electromagnetics wave into DC voltage [1]. The PCE of RF-DC converter plays a crucial role as it determines the overall efficiency of the entire WPT system.

The mismatch between the required parameter values of circuit components and those of the produced ones is inevitable due to numerous imperfections and technological limitations of the relevant manufacturing process. Although the presence the mismatch always affects the performance of device, the contribution of different parameters may differ considerably due to different sensitivity levels. As a result, to reduce the discrepancy between the designed and the actual PCEs one just needs to replace the components whose parameters exhibit the highest sensitivity with ones having lower manufacturing tolerances. Conversely, those components whose tolerances have significantly smaller impact on the device behavior might be replaced with less expensive ones having lower tolerances. Thus, accurate estimation of the parameter sensitivity is of particular practical importance as it allows to identify such low-sensitivity components that may lead to lower device cost.

In the present demo case the following parameters are considered: junction capacitances, series resistances, saturation currents of Schottky diodes, as well as the values of the discrete circuit components (resistors and capacitors).
The estimation of the sensitivity with respect to each model parameter is performed by means of the well-established Monte Carlo method [2].

## Table of contents

[[_TOC_]]


## Problem background

The device under study is a RF-DC converter composed of both discrete circuit and distributed circuit elements operating at 24 GHz. The topology of the converter circuit is the same as the one presented in [3]. The converter contains two Schottky diodes to increase the rectified output voltage and therefore the power conversions efficiency (PCE). The Schottky diodes are employed owing to lower junction capacitance compared to their silicon counterparts that at high frequencies dramatically affects the PCE – the higher the capacitance the lower the PCE. The input BPF filter blocking the higher order harmonic resulting from the nonlinear conversion and the matching network intended to reduce the input reactance seen by the generator (antenna) are realized using microstrip technology as discrete inductor at such high frequencies typically exhibit quite large parasitic capacitance that negatively affects the PCE.
The effect of various parasitic effects of the RF-DC converter PCB on its efficiency cannot be ignored at such high frequencies. To consider them it is convenient to treat the PCE as a separate circuit element - a multiport. Specifically, the original converter circuit may be regarded as a multiport network representing the PBC to each port of which a lumped circuit representing one of the discrete circuit elements or the antenna is connected.
The PCB equivalent circuit is describes using the admittance matrix (Y matrix) parameters. Although the Y matrix elements can be easily found using various approximate models that are highly computationally efficient, in the present demo case study the full-wave analysis of the PCB is performed as it yields more accurate parameter values at the frequencies of higher order harmonics. The full-wave field computations are performed by means of the open source software FENICS [3] employing the finite element method. The Y parameters are extracted from the calculated fields.

<img src="./README_img/DEMO_Fig1.png" alt="Figure 1" width="400"/>

Figure 1: The topology of the RF-DC converter under study [3].

<img src="./README_img/DEMO_Fig2.png" alt="Figure 2" width="400"/>

Figure 2: A shematic view of the structure being modelled [3].


## Contents of this case

The demo case contains files and folders for the calculation of the RF-DC converter power conversion efficiency and performing the Monte Carlo based estimation of the PCE senstitivity to converter circuit parameters.
The files in the repository are arranged as follows:
  * `PMLFEN.py`    – the Python module containing functions for calculation of both the geometric and constitutive parameters of the PML slabs employed to truncate the modelling region. The PML (perfectly matched) layer that effectively absorbs electromagnetics wave impinging upon it is surface regardless of the polarization, frequency shape and propagation direction. In some literature sources it is stated that the minimum distance between the PML surface and the model must not be samller than a quarter wavelength to have little effect on the field calculation accuracy. However, this criterion has been developed for radiating structure, while the structure considered in the present study radiates no field and therefore the distance may be smaller than a half wavelength. 
  * `SPECTRUM.py`    – the Python module containing a number of fnctions peforming conversion of the complex spectral coefficients to the real one and vice versa. Also, the module contains the functions evaluating the derivative in the frequency domain as such an approach to the derivative approximation was found to be more accurate than when the derivatives are approximated directly in the time domain. For the sake of convecience, the function for evaluating instantaneous values of voltages and current at a specific time instant(s) given their phasors is included in this module.
  * `PCE.py`    – the Python function calcuting the PCE for the RF-DC converter comprising two diodes using the Harmonic Balance method that trelies on approximating the circuit voltages and current (for both linear and non-linear element) with truncated Fourier series expansion. Using this approximation and enforcing the net current at each circuit node to zero (according to the Kirchhoff’s Current Law) results in a system of non-linear equations. However, in case of large circuits containing just a few non-linear elements, it would not be wise to solve a large equation system containing as many equations as there are nodes. Fortunately, there is a simple way to reduces the number of equations. .
  * `Jacobian.py`    – the Python module containing functions used to evaluate the Jacobian matrix entries due to the diode current (its contribution), as well as the right hand side vector of the equation systems that is required by the Newton method that solves the original nonlinear equation resulting from the application of the Harmonic Balance method by iteratively constructing and solving approximate linear equation constructed from by using a linearization.
  * `FIELDINTEGRALS.py`    – this Python module contains the following three functions: the function for evaluation of the path integral of the specified function and two functions evaluating the countour integrals (the shape of the coutour in a rectangle). Note that in the current implementation the integration path may only be a straight line segment. 
  * 
  * `postprocessing.py` – Python script used for data post-processing.

  * `README.md`         – this readme file.
  * `README_fig`        – images included in this readme file.


## Model Assumptions 

* The classical SPICE model is used for diode modeling, which takes into account not only the nonlinearity of the capacitance of the p-n transition (or metal-semiconductor transition in the case of Schottky diodes), which plays an important role at high frequencies, but also a number of other parameters.
* In order to reduce the computational burden, the antenna is modeled separately, namely, a separate 3D antenna model is built and analyzed by means of the FENICS finite element method based full-wave electromagnetic field simulation software.
* The developed demo case model allows evaluating the PCE sensitivity for distributed converter circuit components, such as radial stubs, input bandpass filter, matching network and the feedline, as well. However, due to a prohibitively large amount of CPU time needed to estimate the PCE sensitivity with respect these parameters their contribution has been ignored in this demo case study whose results are presented below. 

## References 

1. S. Hemour and K. Wu, "Radio-Frequency Rectifier for Electromagnetic Energy Harvesting: Development Path and Future Outlook," in Proceedings of the IEEE, vol. 102, no. 11, pp. 1667-1691, Nov. 2014, doi: 10.1109/JPROC.2014.2358691.
2. JCGM 101 Supplement 1 to the GUM – Propagation of distributions using Monte Carlo method. (GUM-S1) 2008. Available:http://www.bipm.org/utils/common/documents/jcgm/JCGM_101_2008_E.pdf
3. S. Ladan, A. B. Guntupalli and K. Wu, "A High-Efficiency 24 GHz Rectenna Development Towards Millimeter-Wave Energy Harvesting and Wireless Power Transmission," in IEEE Transactions on Circuits and Systems I: Regular Papers, vol. 61, no. 12, pp. 3358-3366, Dec. 2014, doi: 10.1109/TCSI.2014.2338616.

## Software requirements

The demo case requires Linux environment, as well as the following software:
* Git - to obtain this demo case from GitLab by means of the clone command. 
* Python - needed to run pre- and post- processing Python scripts.
* OpenMPI - required for parallel processing. Versions 2.1.1 and 4.0.0 were tested.

## Obtaining the case

Download the demo model, use the following command to download all the necessary files
```
  git clone https://gitlab.com/eurocc-latvia/estimation-of-rf-to-dc-converter-power-conversion-efficiency-sensitivity-to-component-parameters.git
```

## Running the demo

In case you are not using the RTU HPC, please, recompile the required libraries by first changing your working directory to `$GITPATH/MonteCarloEst` (replace `$GITPATH` with the path to the folder the `Git` repository is cloned into) and executing 
```
  make clean
```
so as to remove precompiled libraries, as well as the relevant objective files, and then compile the librarier from the scratch by issuing 
```
  make all
```
Once the libraries are prepared, you can either start the estimation process, or recalulate the parameters of the rational-polynomial approximation employed to considerably expedite the coputations. Note that the approximation is availbale for the symmetrically located rod only. 

In order to start the uncertainty estimation process, run the runMonteCarlo.sh script
```
  ./runMonteCarlo.sh
```
Upon completion of the script, the estimated mean value (best estimate of the measurand – dielectric constant) and the associated standard deviation are stored in the following file
```
  ESTDATA_ep_x_dr_y_dS11_z	- contains the calculated mean values and relative uncertainties
```
where x,y,z are the values of dielectric constant, uncertainty associated with the radius of the rod, and the one associated with the absolute value of the reflection coefficient (S11). The data from these two files can be exported and plotted using the following Python script:
```
  postproc.py
```
In case of a symmetrically placed rod a significant speed-up in the estimation process can be achieved by accelerating the calculation of the scattering data employing the polynomial-rational approximation. However, to use the approximation the relevant parameters, namely, pole-zero pairs (for constructing the rational part of the approximation) and coefficients of the approximating polynomials (for the polynomial part), must be calculated first. To do so, run the runPolyRat.sh script by executing
```
  ./runPolyCoeff.sh
```
Upon completion of the script, the calculated approximation coefficients are saved in the following two folders:`./poles_zeros` and `./poly_coeff`. Note that for the polynomial approximation the range of the values of the dielectric constant for which the approximation is to be found must be specified, as well as the approximating polynomial degree must be specified. In addition, the number of pole-zero pairs considered by the rational part of the approximation must be specified. 

## User modifications

The user can change the parameters of the structure under consideration, specify the parameters of minimum finding algorithm used to retrieve a dielectric constant from the measured data (synthetic data) and the number of trials for the Monte Carlo method analysis of the uncertainty. These parameters can be found in the `parameter.dat` file.

The parameters of the measurement model are listed below

- eps_r = 30.0
- tan_d = 0.001

- f  	= 10.0
- a  	= 22.86
- xo 	= 0.0

- BNUM = 11

where the parameter `eps_r` must be assigned the value of the dielectric constant of the rod (in case of a lossy dielectric `eps_r` corresponds to the real part of complex dielectric constant), `tan_d` is the loss tangent, `r` is the radius of the rod in mm, `f` is the operating frequency in GHz, `a` – the width of the broader wall of the waveguide and `xo` – is the offset in mm. The parameter `BNUM` corresponds to the number of basis functions used to approximate the fields inside the rod. More specifically, the greater the number of basis functions, the higher the accuracy of the calculated scattering data (S11 and S21). Note, that `BNUM` is used only when S11 and S21 are computed directly without any approximations (see the description of the parameter `APPROX` given below).

The parameters of the objective function minimizing algorithm are as follows

- ACC   = 0.0001
- LEN   = 0.01
- NUMIT = 1000

`ACC` is the convergence threshold for minimizing algorithm, the algorithm terminates when either the value of the objective function becomes less than `ACC`, or the maximum number of iterations (`NUMIT`) is reached. `LEN` is the size of initial search pattern.

The parameters of the Monte Carlo method are

- TRIALS = 1000000
- r_min 	= 1.0
- r_max 	= 10.0
- del_r 	= 0.005
- del_s11 = 0.02

where `TRIALS` indicates the number of trials – the number of times the model outcome is calculated, whereas the parameters `r_min` and `r_max` are intended to specify the lower and the upper limits of range of the rod radius values for which the uncertainty estimation is to be carried out. The other two parameters allow one to set the value of the measurement uncertaintie associated with radius of the rod (`del_r`) in mm, and the absolute value of the reflection coefficient (`del_S11`).

In case you desire to achieve an appreciable speed-in computation, enable the approximated calculations by specifying `yes` in the following line of the `parameters.dat` file

- APPROX = yes

By default the `APPROX` is set to `no` that assumes that the approximation is not used to calculate both S11 and S21.

Should you require to recalulate the polynomial approximation coefficients (the precalculated ones are for the range of `eps_r` from 40 to 90), change the lower and the upper limits of the desired range by specifying `r_min ` and `r_max `, respectively, in the same file. The degree of the approximating polynomials can be changed by assigning the parameter `ORD` a new value. 

## Numerical results

The figures given below show the results of the Monte Carlo sensitivity analysis for the RF-DC converter circuit under study whose topology and schematic view are illustrated in Figs.1 and 2, respectively. The input signal is a pure sine wave with a frequency of 24 GHz. The dimensions of the converter distributed elements, namely, the input band-pass filter and the matching network are chosen in such a way that the PCE is maximum. In order to achieve a significant reduction in the computational burden, the antenna is treated separately from the rest of the circuit. First, the Y parameters of the PCB equivalent circuit are determined by means of FENCIS. Then, these parameters together with antenna impedance also computed using FENCIS and the nominal values of the discrete circuit elements are passed as arguments to the `PCE.py` function to calculate the PCE for a specified range of input power levels or at a single power level. The PCE and the corresponding DC output voltage plotted as functions of the input RF power level in the range from 0.1 mW to 90 mW are shown in Fig.1(a) and (b), respectively. Additionally, the sensitivity analysis has been carried out to determine the contribution of measurement uncertainties or manufacturing tolerances associated with different RF-DC converter circuit parameters to the total PCE discrepancy between the converter PCE value corresponding to the parameter design values and the actual values. However, in the present sensitivity analysis, only the following parameters are considered: $`R_{\mathrm{load}}`$ - the load resistance, $`R_{\mathrm{bias}}`$ - the resistance of the bias resistor, $`R_{\mathrm{d}}`$ - the equivalent series resistance of both Schottky diodes, $`\varepsilon_{\mathrm{r}}`$ - the dielectric permittivity of the substrate (PCB), $`L_{\mathrm{BPF}}`$  - the length of the microstrip band-pass filter intended to suppress higher order harmonics, $`L_{\mathrm{MAT}}`$ - the length of the matching microstrip element employed to ensure the highest power delivered by the antenna to the RF-DC circuit, and $`C_{\mathrm{jo}}`$ - the diode non-linear junction capacitance. The results of the sensitivity analysis, as well as the nominal values of the RF-DC converter circuit parameters along with the relevant uncertainties (tolerances), are summarized in Table 1.

<img src="./README_img/PCE.png" alt="Figure 1a" width="400"/>

<img src="./README_img/Output_Voltage.png" alt="Figure 1b" width="400"/>

Figure 3: The PCE (a) and the DC output voltage (b) as functions of the input power level.

| Parameter Name                | Nominal Value                 | Relative uncertainty      | Relative PCE uncertainty|Sensitivity coefficient|
| ----------------------------- |:-----------------------------:|:-------------------------:|:--------:|:--------:|
| $`R_{\mathrm{load}}`$	          | $`160\,\Omega`$	                | $`5.0`$	                  | $`0.0528`$|$`0.0106`$ |
| $`R_{\mathrm{bias}}`$	          | $`100\,\Omega`$	                | $`5.0`$	                  | $`5.3365`$|$`1.0673`$ |
| $`R_{\mathrm{d}}`$	            | $`4\,\Omega`$	                  | $`50`$	                  | $`2.2797`$|$`0.0456`$ |
| $`\varepsilon_{\mathrm{r}}`$	  | $`2.2\,(\text{Duroid})`$	      | $`13.64`$	                | $`32.42`$ |$`2.3780`$ |
| $`L_{\mathrm{BPF}}`$	          | $`5.1\,\mathrm{mm}`$	          | $`13.64`$	                | $`32.42`$ |$`0.0695`$ |
| $`L_{\mathrm{MAT}}`$	          | $`4.7\,\mathrm{mm}`$	          | $`0.64 (30\mathrm{\mu m})`$| $`2.699`$ |$`4.2287`$ |
| $`C_{\mathrm{jo}}`$	            | $`0.02\,\mathrm{pF}`$	          | $`50`$	                  | $`6.864`$ |$`0.1373`$ |

