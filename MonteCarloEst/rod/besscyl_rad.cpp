#include<stdio.h>
#include<malloc.h>
#include<omp.h> 
#include<complex.h>


#define PI 3.141592653589793238462
#define cdouble  _Complex double


void besselj(int, cdouble, cdouble*, int);
void bessely(int, cdouble, cdouble*, int);
 

#define BJp(i)  BJp[i-1]

#define gam(i)  gampt[0][i-1]
#define gamp(i) gampt[1][i-1]

#define BJo(i)  BJ[0][i-1]
#define BYo(i)  BJ[1][i-1]


void besscyl_rad(cdouble eps, double r, double al, cdouble * BJ[], cdouble * gampt[], const int M){


	int N = 2*M;
	int na = N, n, m;

	cdouble BJp[N+1];
	cdouble kpr;

	cdouble sqreps = csqrt(eps);
	cdouble isqreps = 1.0/sqreps;

	kpr = 2.0*PI*sqreps*r*al;

	besselj(0, kpr, BJp, N+1);

	for(n=1; n<=N; ++n){
		gam(n)  =  isqreps*BJo(n+1)*BJp(n) - BJo(n)*BJp(n+1);
		gamp(n) =  BYo(n)*BJp(n+1) - isqreps*BYo(n+1)*BJp(n);
	}

}

