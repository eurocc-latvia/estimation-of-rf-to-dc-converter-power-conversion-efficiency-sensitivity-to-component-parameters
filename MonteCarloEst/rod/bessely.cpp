#include<complex.h>


//#define PI 3.141592653589793238462
#define cdouble  _Complex double


extern "C" void zbesy_(double*,double*,double*,int*,int*,double*,double*,int*,double*,double*,int*);   


void bessely(int n, cdouble z, cdouble *c,int na){

int kode=1,nz,ierr;

double cr[na],ci[na];
double cwr[na],cwi[na];
double zr,zi,nd = (double) n;

double * crp = (double *) c;
double * cip = crp + 1;

zr = creal(z);
zi = cimag(z);

zbesy_(&zr,&zi,&nd,&kode,&na,cr,ci,&nz,cwr,cwi,&ierr);

for(int k=0;k<na;++k){

  *crp=cr[k];
  *cip=ci[k];

  crp += 2; 
  cip += 2; 

}

}
