#include<stdio.h>
#include<malloc.h>
#include<math.h>
#include<complex.h>


#define PI 3.1415926535897932384626433832795028


#define t(i) t[i-1]
#define coef(i) coef[i-1]
#define E(i) E[i-1]
#define D(i) D[i-1]
#define Z(i) Z[i-1]

#define ph(i) ph[i-1]
#define ww(i) ww[i-1]



extern "C" void dstev_(char*,int*,double*,double*,double*,int*,double*,int*);


void Legendre(int IP, double * ph, double * ww){


int N = IP, LDZ = IP,INFO = 0,n;
char JOBZ='V';


double E[N-1], Z[N*LDZ], WORK[2*N-2];
double t[N+2], coef[N-1];

double * D = (double *) calloc(sizeof(double),N);


t(1)=1.0;
t(2)=1.0;

for(n=1;n<=N;++n){
   t(n+2) = t(n+1)*(2*n+1)/(n+1);
}

for(n=1;n<N;++n){
   coef(n) = t(n)/t(n+1)*n/(2*n+1);
}

for(n=1;n<N;++n){
    E(n) = sqrt(coef(n));
}

dstev_(&JOBZ,&N,D,E,Z,&LDZ,WORK,&INFO);

for(int j=1;j<=N;++j){
    ph(j)=0.25*PI*(D[j-1]+1.0);
    ww(j)=0.25*PI*2.0*Z[(j-1)*N]*Z[(j-1)*N];
}

}

/*
int main(int argc, char ** argv){

double ww[19];
double ph[19];

Legendre(19, ph, ww);

for(int n = 0; n < 19; ++n){
//    printf("%5.19e \n", ph[n]);
}

return 1.0;

}
*/
