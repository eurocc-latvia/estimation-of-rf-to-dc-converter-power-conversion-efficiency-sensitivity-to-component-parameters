#include<stdio.h>
#include<malloc.h>
#include<math.h>
#include<complex.h>


#define PI 3.141592653589793238462


#define cosn1(i,j) cosn[0][i-1+(j-1)*IP]
#define cosn2(i,j) cosn[1][i-1+(j-1)*IP]

#define ph(i) wph[0][i-1]
#define ww(i) wph[1][i-1]
#define sn(i) wph[2][i-1]
#define cn(i) wph[3][i-1]

#define phPt  wph[0]
#define wwPt  wph[1]


void Legendre(int,double*,double*);


void cosin(double * wph[], double * cosn[], const int M, const int IP){

int n,m; 

double ph[IP],ww[IP];

Legendre(IP,phPt,wwPt);


for(int n=1;n<=M;++n){
   for(int m=1;m<=IP;++m){
    
   if(n==1){      
      sn(m) = sin(ph(m));
      cn(m) = cos(ph(m));
   } 
      cosn1(m,n) = cos(ph(m)*(2*n-2));
      cosn2(m,n) = cos(ph(m)*(2*n-1));

   }
}

}

