#include<complex.h>

#define cdouble _Complex double


extern "C" void zbesj_(double*, double*, double*, int*, int*, double*, double*, int*, int*);


void besseljCPX(int n, cdouble z, cdouble *c, int na){

	int kode=1, nz, ierr;

	double cr[na], ci[na];
	double zr, zi, nd = static_cast<double>(n);

	zr = creal(z);
	zi = cimag(z);

	zbesj_(&zr, &zi, &nd, &kode, &na, cr, ci, &nz, &ierr);

	for(int k=0; k<na; ++k){
		  c[k] = cr[k] + I*ci[k];
	}

}
