#!/bin/bash

MPATH=/mnt/home/romanetf/GIT_HPC/dielectricconstantmeasuncertainty/MonteCarloEst

cd $MPATH

rm -f ./PoleZeroInit

mpic++  PoleZeroInit.cpp -o PoleZeroInit -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran -DMP

mpirun ./PoleZeroInit

cd ~
